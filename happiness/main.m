//
//  main.m
//  happiness
//
//  Created by Aakash Apoorv on 15/10/15.
//  Copyright © 2015 Aakash Apoorv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
