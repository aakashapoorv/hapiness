//
//  HappyViewController.h
//  happiness
//
//  Created by Aakash Apoorv on 15/10/15.
//  Copyright © 2015 Aakash Apoorv. All rights reserved.
//

#import "HappyViewController.h"

@interface HappyViewController ()
@end

@implementation HappyViewController

bool allowNotif;
bool allowsSound;
bool allowsBadge;
bool allowsAlert;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self registerLocalNotification];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) registerLocalNotification
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    [self setNotificationTypesAllowed];
    if (notification)
    {
        if (allowNotif)
        {
            notification.fireDate = [[NSDate date] dateByAddingTimeInterval:60*60]; // 3600 seconds form now
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.repeatInterval = NSCalendarUnitHour;
        }
        if (allowsAlert)
        {
            notification.alertBody = @"Are you happy? Let us know how happy you are :)";
        }
        if (allowsBadge)
        {
            notification.applicationIconBadgeNumber = 1;
        }
        if (allowsSound)
        {
            notification.soundName = UILocalNotificationDefaultSoundName;
        }
        
        // this will schedule the notification to fire at the fire date
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        // this will fire the notification right away, it will still also fire at the date we set
        //[[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

- (void)setNotificationTypesAllowed
{
    NSLog(@"%s:", __PRETTY_FUNCTION__);
    // get the current notification settings
    UIUserNotificationSettings *currentSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    allowNotif = (currentSettings.types != UIUserNotificationTypeNone);
    allowsSound = (currentSettings.types & UIUserNotificationTypeSound) != 0;
    allowsBadge = (currentSettings.types & UIUserNotificationTypeBadge) != 0;
    allowsAlert = (currentSettings.types & UIUserNotificationTypeAlert) != 0;
}

- (IBAction)submitSurvey:(id)sender
{
    // get current date and time
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSString *dateString = [dateFormat stringFromDate:today];
    NSLog(@"date: %@", dateString);
    
    NSString *surveryScoreWithDate = [NSString stringWithFormat:@"%@ - %@", dateString, [self.happyScore titleForSegmentAtIndex:self.happyScore.selectedSegmentIndex]];
    [self saveSurveyData:surveryScoreWithDate];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Happiness score submited" message:@"Come back after one hour :)" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

- (void) saveSurveyData:(NSString *) survey
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"HappySurvey.txt"];
    NSString *previousValues = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSString *currentValue;
    
    // Check if file have no date
    if(previousValues == NULL)
        currentValue = survey;
    else
        currentValue = [NSString stringWithFormat:@"%@\n%@", survey, previousValues];
    
    // Save currentValue to file
    [currentValue writeToFile:filePath atomically:TRUE encoding:NSUTF8StringEncoding error:NULL];
    
    // Log data in Console
    NSLog(@"%s: previousValues = %@", __PRETTY_FUNCTION__, previousValues);
    NSLog(@"%s: currentValue = %@", __PRETTY_FUNCTION__, currentValue);
}

@end

