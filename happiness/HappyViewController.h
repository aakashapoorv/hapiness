//
//  HappyViewController.h
//  happiness
//
//  Created by Aakash Apoorv on 15/10/15.
//  Copyright © 2015 Aakash Apoorv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HappyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *happyScore;
@end

